Microservicio de Clientes

Proyecto de curso JAVA DevOps

Endpoints:
    - Listar todos los clientes   -> /api/clientes
    - Listar un cliente por ID    -> /api/cliente/{id}
    - Eliminar un cliente por ID  -> /api/cliente/{id}
    - Guardar un cliente          -> /api/cliente
    - Actualizar un cliente       -> /api/cliente
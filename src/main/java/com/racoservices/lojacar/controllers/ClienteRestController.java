package com.racoservices.lojacar.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.racoservices.lojacar.entity.Cliente;
import com.racoservices.lojacar.services.IClienteService;

@RestController
@RequestMapping("/api")
public class ClienteRestController {

	@Autowired
	private IClienteService clienteService;
	
	@RequestMapping(value="/clientes", method = RequestMethod.GET, produces = "application/json;charset=utf8")
	public List<Cliente> findAll(){
		return clienteService.findAll();
	}
	
	@GetMapping("/cliente/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Cliente findById(@PathVariable Long id) {
		return clienteService.findById(id);
	}
	
	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clienteService.delete(id);
	}
	
	@PostMapping("/cliente")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Cliente save(@RequestBody Cliente cliente) {
		return clienteService.save(cliente);
	}
	
	@PutMapping("/cliente")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Cliente update(@RequestBody Cliente cliente, @PathVariable Long id) {
		//Primero se obtiene el cliente
		Cliente clienteActual = clienteService.findById(id);
		
		clienteActual.setApellidos(cliente.getApellidos());
		clienteActual.setNombres(cliente.getNombres());
		clienteActual.setEmail(cliente.getEmail());
		
		return clienteService.save(clienteActual);
	}
	
}

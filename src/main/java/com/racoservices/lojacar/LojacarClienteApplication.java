package com.racoservices.lojacar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LojacarClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(LojacarClienteApplication.class, args);
	}

}

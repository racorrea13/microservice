package com.racoservices.lojacar.services;

import java.util.List;

import com.racoservices.lojacar.entity.Cliente;

public interface IClienteService {
	
	public List<Cliente> findAll();
	
	public Cliente findById(Long id);
	
	public Cliente save(Cliente cliente);
	
	public void delete(Long id);

}

package com.racoservices.lojacar.dao;

import org.springframework.data.repository.CrudRepository;

import com.racoservices.lojacar.entity.Cliente;

public interface IClienteDao extends CrudRepository<Cliente, Long> {

}
